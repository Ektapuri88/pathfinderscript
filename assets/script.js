$( "#findnow" ).click(function() {
	var start = $("#start").val();
	var end = $("#end").val()
	var regex = new RegExp("^[a-eA-e]+$");
	if (regex.test(start) && regex.test(end)) 
	{
		$.ajax({
			type: 'POST',
			url: 'handler.php',
			data:'start='+start+'&end='+end+'&findpath=1',
			beforeSend: function () {
				$('.loading').removeClass('hide');
			},
			success: function (html) {
				$('.error').addClass('hide');
				$('#output').removeClass('hide');
				$('#output').html(html);
				$('.loading').addClass("hide");
			}
		});
	}
	else
	{
		$('.loading').addClass("hide");
		$('#output').addClass("hide");
		$('.error').removeClass('hide');
		$('.error').html("Something went wrong, Please enter valid Node. Only A to E nodes are accepted.");
	}
});