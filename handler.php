<?php
require_once 'pathfinder.php';
if(!empty( $_POST['findpath'] ) && $_POST['findpath']==1) {
    $myPath = new pathfinder();
    $start = $_POST['start'];
    $end = $_POST['end'];
    if(!empty($start) && !empty($end) && preg_match("#^[a-eA-E]{0,1}$#",$start) && preg_match("#^[a-eA-E]{0,1}$#",$end)){
    	$result = $myPath->get_path(strtoupper($start),strtoupper($end));
	    echo $result;
    } else{
    	echo 'Something went wrong, Please enter valid Node. Only A to E nodes are accepted.';
    }
}
?>