<?php
/*
This is a basic algorithm which is converted to the working function this will return all the possible paths and distance without repeating the nodes.

Algorithm 
  Step 1: arrange array to get all the possible end nodes from one start node
  Step 2: create a loop to check each endnode of the start node
  Step 3: create a loop to get further connection to the required end node and calculate distance
  Step 4: repeat step 3
  Step 5: arrange array as to have path and distance as key and value
  Step 6: sort array in ascending order
  Step 7: end
*/
class pathfinder
{
  public function get_path($start,$end)// nodes coming from form
  {
    $distance_array = [
        "0"=>["1"=>"A","2"=>"B","3"=>"5"],
        "1"=>["1"=>"A","2"=>"C","3"=>"9"],
        "2"=>["1"=>"A","2"=>"D","3"=>"10"],
        "3"=>["1"=>"A","2"=>"E","3"=>"29"],
        "4"=>["1"=>"B","2"=>"C","3"=>"9"],
        "5"=>["1"=>"B","2"=>"D","3"=>"11"],
        "6"=>["1"=>"B","2"=>"E","3"=>"13"],
        "7"=>["1"=>"C","2"=>"D","3"=>"1"],
        "8"=>["1"=>"C","2"=>"E","3"=>"9"],
        "9"=>["1"=>"D","2"=>"E","3"=>"10"]
      ];
      
      $get_all_possibilities=[];//get all the possible end nodes from each start node
      foreach($distance_array as $key=>$value)
      {
        $val_arr['node2']=$value[2];
        $val_arr['distance']=$value[3];
        $get_all_possibilities[$value[1]][] =$val_arr; 

        $val_arr2['node2']=$value[1];
        $val_arr2['distance']=$value[3];
        $get_all_possibilities[$value[2]][] =$val_arr2; 
      }
      $get_paths = $this->get_all_possibilities($get_all_possibilities,$start,$end);
      $result= "<h2>Possible paths from ".$start." to ".$end."</h2><br/>";
      if(!empty($get_paths))
      {
        foreach($get_paths as $path=>$distance)
        {
          if($path)
            $result.= "<p>Path ".$path." has total distance ".$distance."</p>";
        }
      }
      else
        $result.= "<h2>Sorry No Path Found, Try Again!</h2>";

      echo $result;
  }
  private function get_all_possibilities($paths,$start,$end)
  {
    $get_path=[];
    $return_array=[];
    foreach($paths[$start] as $nodeValue)  { 
        if($nodeValue['node2']!=$end && $nodeValue['node2']!=$start) {
          $distance = $nodeValue['distance'];
          $checkPath[][]=$this->findPath($paths,$nodeValue['node2'],$start,$end,$start.'->'.$nodeValue['node2'],$distance);
        }
        else if($nodeValue['node2']==$end){
          $checkPath[$start.'->'.$end] = $nodeValue['distance'];
        }
    }
    $arr=$this->array_flatten($checkPath);
    asort($arr);
    return $arr;
  }
  //use recursion to get all posibilities
  private function findPath($paths,$way,$start,$end,$cmgPath,$distance,$middle_ways=[])
  {
    $possiblePaths=[];
    if(!empty($paths[$way])) {
      if(!in_array($way, $middle_ways)) {
        foreach($paths[$way] as $wayNodeS=>$wayNodeValue) {
          if($wayNodeValue['node2']==$end && $wayNodeValue['node2']!=$start) {
              $cmgPath=$cmgPath.'->'.$end;
              $distance = $distance+$wayNodeValue['distance'];
              $possiblePaths[$cmgPath]=$distance;
              break;
          } else  {
            if($wayNodeValue['node2']!=$start)  {
              $middle_ways[]=$wayNodeValue['node2'];
              $possiblePaths[] = $this->findPath($paths,$wayNodeValue['node2'],$start,$end,$cmgPath.'->'.$wayNodeValue['node2'],$distance,$middle_ways);
            }
          }
        }
      }
    }
    return $possiblePaths;
  }
  //Convert a multi-dimensional array into a single-dimensional array.
  private function array_flatten($array) { 
     $result = array();
      foreach($array as $key=>$value) {
          if(is_array($value)) {
              $result = $result + $this->array_flatten($value, $key);
          }
          else {
              $result[$key] = $value;
          }
      }
      return $result;
  } 
}
