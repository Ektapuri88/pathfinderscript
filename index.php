<!DOCTYPE html>
<html lang="en">
<head>
  <title>Path Finder Script</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav"><!--Side Nav to show predefined nodes accpeted by script-->
      <h4>Defined Paths</h4>
      <p>These are two way paths, A->B is same as B->A</p>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="">Node A <--> Node B (5 KM)</a></li>
        <li><a href="">Node A <--> Node C (9 KM)</a></li>
        <li><a href="">Node A <--> Node D (10 KM)</a></li>
        <li><a href="">Node A <--> Node E (29 KM)</a></li>
        <li><a href="">Node B <--> Node C (9 KM)</a></li>
        <li><a href="">Node B <--> Node D (11 KM)</a></li>
        <li><a href="">Node B <--> Node E (13 KM)</a></li>
        <li><a href="">Node C <--> Node D (1 KM)</a></li>
        <li><a href="">Node C <--> Node E (9 KM)</a></li>
        <li><a href="">Node D <--> Node E (10 KM)</a></li>
      </ul>
    </div><!-- side nav ends -->
    <div class="col-sm-9">
      <h4>Find Path Between Two Nodes:</h4>
      <form role="form">
        <div class="form-group">
          <label>From: </label>
          <input type="text" maxlength="1" name="start" id="start" value="" placeholder="A" class="inputStyle">
        </div>
        <div class="form-group">
          <label>To: </label>
          <input type="text" maxlength="1" name="end" id="end" value="" placeholder="E" class="inputStyle">
        </div>
        <button type="button" id="findnow" class="btn btn-success">Find</button>
      </form>
      <hr>
      <div class="error hide"></div>
      <div class="loading hide">Loading ... Please wait</div>
      <div id="output" class="hide"><!-- output will be displayed here --></div>
    </div>
  </div>
</div>
<footer class="container-fluid">
  <script src="assets/script.js"></script>
  <p>by Ekta Puri</p>
</footer>
</body>
</html>
